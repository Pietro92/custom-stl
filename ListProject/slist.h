#pragma once
#include <iterator>
#include<algorithm>
#include "node.h"

#pragma region Iterators
template<typename T>
class SListIterator : public std::iterator<std::forward_iterator_tag, Node<T>> {
private:
	Node<T>* mNode = 0;
	template<typename T> friend class SList;
	template<typename T> friend class SListConstIterator;

public:
	SListIterator(Node<T>* nNode) : mNode(nNode) {}
	SListIterator(const SListIterator& nIterator) : mNode(nIterator.mNode) {}

	SListIterator& operator++()
	{
		mNode = mNode->mNext;
		return *this;
	}

	SListIterator operator++(int)
	{
		SListIterator tempListIterator(*this);
		operator++();
		return tempListIterator;
	}

	bool operator==(const SListIterator& other) const
	{
		return mNode == other.mNode;
	}

	bool operator!=(const SListIterator& other) const
	{
		return mNode != other.mNode;
	}

	T& operator*() const
	{
		return mNode->mData;
	}
};

template<typename T>
class SListConstIterator : public std::iterator<std::forward_iterator_tag, Node<T>> {
private:
	Node<T>* mNode = 0;
	template<typename T> friend class SList;

public:
	SListConstIterator(Node<T>* nNode) : mNode(nNode) {}
	SListConstIterator(const SListConstIterator& nIterator) : mNode(nIterator.mNode) {}

	SListConstIterator& operator++()
	{
		mNode = mNode->mNext;
		return *this;
	}

	SListConstIterator operator++(int)
	{
		SListConstIterator tempListIterator(*this);
		operator++();
		return tempListIterator;
	}

	bool operator==(const SListConstIterator& other) const
	{
		return mNode == other.mNode;
	}

	bool operator!=(const SListConstIterator& other) const
	{
		return mNode != other.mNode;
	}

	const T& operator*() const
	{
		return mNode->mData;
	}
};
#pragma endregion Iterators

template<typename T>
class SList {
public:
	using iterator = SListIterator<T>;
	using const_iterator = SListConstIterator<T>;

private:
	Node<T>* mHead = 0;

public:
	// Constructors and Destructor
	SList() {}
	SList(std::initializer_list<T> init) 
	{
		assign(init.begin(), init.end());
	}
	~SList()
	{
		delete mHead;
	}

	// Member functions
	SList<T>& operator=(std::initializer_list<T> init)
	{
		assign(init.begin(), init.end());
		return *this;
	}

	template<class InputIt>
	void assign(InputIt first, InputIt last)
	{
		clear();

		// Make the first node and set on head
		Node<T>* nNode = new Node<T>(*first);
		mHead = nNode;
		first++;

		// Make all next nodes
		while (first != last)
		{
			nNode->mNext = new Node<T>(*first);
			nNode = nNode->mNext;
			first++;
		}
	}

	void assign(std::size_t n, const T& val)
	{
		clear();
		if (n == 0)	return;

		Node<T>* nNode = new Node<T>(val);
		mHead = nNode;
		if (n == 1)	return;

		for (int i = 1; i < n; i++)
		{
			nNode->mNext = new Node<T>(val);
			nNode = nNode->mNext;
		}
	}

	void assign(std::initializer_list<T> ilist)
	{
		clear();
		auto it = ilist.begin();
		if (it == ilist.end())	return;

		Node<T>* nNode = new Node<T>(*it);
		it++;

		mHead = nNode;
		for (; it != ilist.end(); it++)
		{
			nNode->mNext = new Node<T>(*it);
			nNode = nNode->mNext;
		}
	}

	// Element access
	T& front()
	{
		return mHead->mData;
	}

	// Iterators
	iterator before_begin()
	{
		Node<T>* nNode = new Node<T>(T(), mHead);
		return iterator(nNode);
	}

	const_iterator cbefore_begin()
	{
		Node<T>* nNode = new Node<T>(T(), mHead);
		return const_iterator(nNode);
	}

	iterator begin()
	{
		return iterator(mHead);
	}

	const_iterator cbegin()
	{
		return const_iterator(mHead);
	}

	iterator end()
	{
		return iterator(nullptr);
	}

	const_iterator cend()
	{
		return const_iterator(nullptr);
	}

	// Capacity
		bool empty()
	{
		return mHead == nullptr;
	}

	std::size_t max_size() const noexcept
	{
		return std::distance(begin(), end());
	}

	// Modifiers
	void clear()
	{
		Node<T>* firstNode = mHead;
		while (firstNode != nullptr)
		{
			Node<T>* tempNode = firstNode;
			firstNode = firstNode->mNext;
			delete tempNode;
		}
		mHead = nullptr;
	}

	void push_front(const T& value)
	{
		Node<T>* nNode = new Node<T>(value, mHead);
		mHead = nNode;
	}

	void pop_front()
	{
		Node<T> oldNode = mHead;
		mHead = mHead->mNext;
		delete oldNode;
	}

	iterator insert_after(const_iterator pos, const T& value)
	{
		Node<T>* afterNode = pos.mNode->mNext;
		pos.mNode->mNext = new Node<T>(value, afterNode);
		return iterator(pos.mNode->mNext);
	}

	iterator insert_after(const_iterator pos, T&& value)
	{
		Node<T>* afterNode = pos.mNode->mNext;
		pos.mNode->mNext = new Node<T>(value, afterNode);
		return iterator(pos.mNode->mNext);
	}

	iterator insert_after(const_iterator pos, std::size_t count, const T& value)
	{
		Node<T>* afterNode = pos.mNode->mNext;
		Node<T>* nNode = pos.mNode;
		for (int i = 0; i < count; i++)
		{
			nNode->mNext = new Node<T>(value);
			nNode = nNode->mNext;
		}
		nNode->mNext = afterNode;
		return iterator(pos.mNode->mNext);
	}

	template<class InputIt>
	iterator insert_after(const_iterator pos, InputIt first, InputIt last)
	{
		Node<T>* afterNode = pos.mNode->mNext;
		Node<T>* nNode = pos.mNode;
		while (first != last)
		{
			nNode->mNext = new Node<T>(*first);
			nNode = nNode->mNext;
			first++;
		}
		nNode->mNext = afterNode;
		return iterator(pos.mNode->mNext);
	}

	iterator insert_after(const_iterator pos, std::initializer_list<T> ilist)
	{
		Node<T>* afterNode = pos.mNode->mNext;
		Node<T>* nNode = pos.mNode;
		for (auto it = ilist.begin(); it != ilist.end(); it++)
		{
			nNode->mNext = new Node<T>(*it);
			nNode = nNode->mNext;
		}
		nNode->mNext = afterNode;
		return iterator(pos.mNode);
	}		

	iterator erase_after(const_iterator pos)
	{
		if (pos.mNode->mNext == nullptr)	return iterator(nullptr);

		Node<T>* nNode = pos.mNode->mNext;
		pos.mNode->mNext = pos.mNode->mNext->mNext;
		delete nNode;
		return iterator(pos.mNode->mNext);
	}

	iterator erase_after(const_iterator first, const_iterator last)
	{
		if (first == last) return iterator(last.mNode);

		Node<T>* beginNode = first.mNode;
		first++;
		while (first != last)
		{
			Node<T>* nNode = first.mNode;
			first++;
			delete nNode;
		}
		beginNode->mNext = last.mNode;
		return iterator(last.mNode);
	}

	template<class... Args>
	void emplace_front(Args&&... args)
	{
		T elem(args...);
		Node<T>* newNode = new Node<T>(elem, mHead);
		mHead = newNode;
	}

	template< class... Args >
	iterator emplace_after(const_iterator pos, Args&&... args)
	{
		Node<T>* aftetNode = pos.mNode->mNext;
		T elem(args...);
		pos.mNode->mNext = new Node<T>(elem, aftetNode);
		return iterator(pos.mNode->mNext);
	}

	void resize(std::size_t n)
	{
		resize(n, T());
	}

	void resize(std::size_t n, const T& val)
	{
		Node<T>* nNode = mHead;
		int index = 0;
		if (mHead != nullptr)
		{
			for (index = 1; index < n; index++)
			{
				nNode = nNode->mNext;
			}
		}
		if (index < n)
		{
			insert_after(const_iterator(nNode), n - index, val);
		}
		else
		{
			erase_after(const_iterator(nNode), cend());
		}
	}

	void swap(SList<T>& list)
	{
		Node<T>* tempNode = mHead;
		mHead = list.mHead;
		list.mHead = tempNode;
	}

	// Operations
	void merge(SList<T>& other) 
	{
		// select the last element on list before nullptr
		Node<T>* lastNode = mHead;
		while (lastNode->mNext != nullptr)
		{
			lastNode = lastNode->mNext;
		}

		// merge the elements on second list to first list
		for (auto it = other.cbegin(); it != other.cend(); it++)
		{
			lastNode->mNext = new Node<T>(*it);
			lastNode = lastNode->mNext;
		}

		// order all elements after merge
		sort();	
	}

	void merge(SList<T>&& other)
	{
		merge(other);
	}

	void splice_after(const_iterator pos, SList<T>& other)
	{
		insert_after(pos, other.cbegin(), other.cend());
		other.clear();
	}

	void splice_after(const_iterator pos, SList<T>&& other)
	{
		insert_after(pos, other.cbegin());
		other.clear();
	}

	void splice_after(const_iterator pos, SList<T>& other, const_iterator first, const_iterator last)
	{
		auto it = first;
		it++;
		insert_after(pos, it, last);
		last.erase_after(first, last);
	}

	void splice_after(const_iterator pos, SList<T>&& other, const_iterator first, const_iterator last)
	{
		auto it = first;
		it++;
		insert_after(pos, it, last);
		last.erase_after(first, last);
	}

	void remove(const T& val)
	{
		auto unaryPred = [&](const T& elem) {
			return elem == val;
		};
		remove_if(unaryPred);
	}

	template<class UnaryPredicate>
	void remove_if(UnaryPredicate pred)
	{
		Node<T>* nNode = mHead;
		while (nNode && mHead == nNode && pred(mHead->data))
		{
			nNode = nNode->next;
			delete mHead;
			mHead = nNode;
		}

		if (!nNode) return;

		while (nNode->next)
		{
			if (pred(nNode->next->data))
			{
				Node<T>* tempNode = nNode->next;
				nNode->next = nNode->next->next;
				delete tempNode;
			}
			else
			{
				nNode = nNode->next;
			}
		}
	}

	void unique()
	{
		auto binary_pred = [](const T& a, const T& b) { return a == b; };
		unique(binary_pred);
	}

	template<class BinaryPredicate>
	void unique(BinaryPredicate binary_pred)
	{
		const_iterator it(mHead);
		const_iterator it2(it);
		it2++;
		
		while (it2 != cend())
		{
			if (binary_pred(*it, *it2))
			{
				erase_after(it, ++it2);
			}
			else
			{
				it = it2;
				it2++;
			}
		}
	}

	void reverse()
	{
		if(!mHead || mHead->mNext)	return;

		Node<T>* newNode = new Node<T>(mHead->mNext);
		for (Node<T>* tempNode = mHead->mNext; tempNode != nullptr; tempNode = tempNode->mNext)
		{
			newNode = new Node<T>(tempNode->mData, newNode);
		}
		clear();
		mHead = newNode;
	}

	void sort()
	{
		auto comp = [](T& a, T& b) { return a > b; };
		sort(comp);
	}

	template<class Compare>
	void sort(Compare comp)
	{
		if (begin() == end())	return;

		for (auto it = begin(); it != end(); it++)
		{
			for (auto it2 = it; it2 != end(); ++it2)
			{
				if (comp(*it, *it2))
				{
					T elem = *it;
					*it = *it2;
					*it2 = elem;
				}
			}
		}
	}
};