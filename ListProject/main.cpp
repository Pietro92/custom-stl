#include<iostream>
#include"slist.h"

/*
SList<int> mListSample({ 1, 2, 3, 4 });
SList<int> mListSample2({ 199, 188, 28, 1 });
SList<int>::const_iterator it2 = mListSample.cbegin();
it2++;
mListSample.insert_after(it2, mListSample2.cbegin(), mListSample2.cend());

for (SList<int>::iterator it = mListSample.begin(); it != mListSample.end(); it++)
{
	std::cout << *it << std::endl;
}

std::cout << "************************************************" << std::endl;

mListSample.erase_after(it2);
for (SList<int>::iterator it = mListSample.begin(); it != mListSample.end(); it++)
{
	std::cout << *it << std::endl;
}*/

template<typename T>
void print_slist(SList<T>& mList)
{
	for (auto it = mList.cbegin(); it != mList.cend(); it++)
	{
		std::cout << *it << std::endl;
	}
}

int main()
{
	SList<int> mListSample({ 1, 2, 3, 4 });
	SList<int> mListSample2({ 199, 188, 28, 1 });
	auto it = mListSample.cbegin();
	mListSample.splice_after(it, mListSample2);

	print_slist(mListSample);

	return 0;
}