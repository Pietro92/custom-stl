#pragma once

template<typename T>
struct Node {
	T mData;
	Node<T>* mNext;

	Node(const T& nData, Node<T>* nNext = nullptr) : mData(nData), mNext(nNext) {}
};